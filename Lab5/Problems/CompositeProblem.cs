using System.Collections.Generic;
using System.Linq;
using ResultsCombiners;
using Solvers;

namespace Problems
{
    internal class CompositeProblem : Problem
    {
        private readonly IEnumerable<Problem> _problems;
        private readonly IResultsCombiner _resultsCombiner;

        public CompositeProblem(string name, IEnumerable<Problem> problems,
            IResultsCombiner resultsCombiner) : base(name, () => 0)
        {
            _problems = problems;
            _resultsCombiner = resultsCombiner;
        }

        public override void TrySolveWithThis(ISolver solver)
        {
            foreach (var problem in _problems)
            {
                if (problem.Solved) continue;
                solver.TrySolve(problem);
            }

            if (_problems.All(p => p.Solved))
                TryMarkAsSolved(_resultsCombiner.CombineResults(_problems.Select(p => p.Result.Value)));
        }
    }
}