using System;
using Solvers;

namespace Problems
{
    internal class NetworkProblem : Problem
    {
        public NetworkProblem(string name, Func<int> computation, int dataToTransfer) : base(name, computation)
        {
            DataToTransfer = dataToTransfer;
        }

        public int DataToTransfer { get; }

        public override void TrySolveWithThis(ISolver solver)
        {
            var res = solver.TrySolve(this);
            TryMarkAsSolved(res);
        }
    }
}