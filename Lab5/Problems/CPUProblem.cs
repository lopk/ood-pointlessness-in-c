using System;
using Solvers;

namespace Problems
{
    internal class CPUProblem : Problem
    {
        public CPUProblem(string name, Func<int> computation, int requiredThreads) : base(name, computation)
        {
            RequiredThreads = requiredThreads;
        }

        public int RequiredThreads { get; }

        public override void TrySolveWithThis(ISolver solver)
        {
            var res = solver.TrySolve(this);
            TryMarkAsSolved(res);
        }
    }
}