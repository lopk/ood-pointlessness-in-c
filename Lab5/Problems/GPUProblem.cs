using System;
using Solvers;

namespace Problems
{
    internal class GPUProblem : Problem
    {
        public GPUProblem(string name, Func<int> computation, int gpuTemperatureIncrease) : base(name, computation)
        {
            GpuTemperatureIncrease = gpuTemperatureIncrease;
        }

        public int GpuTemperatureIncrease { get; }

        public override void TrySolveWithThis(ISolver solver)
        {
            var res = solver.TrySolve(this);
            TryMarkAsSolved(res);
        }
    }
}