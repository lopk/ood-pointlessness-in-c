using System.Collections.Generic;

namespace ResultsCombiners
{
    internal interface IResultsCombiner
    {
        int CombineResults(IEnumerable<int> results);
    }
}