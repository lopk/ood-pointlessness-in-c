using System.Collections.Generic;
using System.Linq;

namespace ResultsCombiners
{
    internal class SumResultsCombiner : IResultsCombiner
    {
        public int CombineResults(IEnumerable<int> results)
        {
            return results.Sum();
        }
    }
}