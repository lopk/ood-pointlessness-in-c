using System;
using Problems;

namespace Solvers
{
    internal class GPU : ISolver
    {
        private readonly int _coolingFactor;
        private int _temperature;
        private readonly string _model;

        public GPU(string model, int temperature, int coolingFactor)
        {
            _model = model;
            _temperature = temperature;
            _coolingFactor = coolingFactor;
        }

        private static int MaxTemperature { get; } = 100;
        private static int CPUProblemTemperatureMultiplier { get; } = 3;


        string ISolver.Model => _model;

        int? ISolver.TrySolve(CPUProblem problem)
        {
            var requiredTemperature = problem.RequiredThreads * CPUProblemTemperatureMultiplier;
            if (DidThermalThrottle()) return null;
            _temperature += requiredTemperature;
            return problem.Computation();
        }

        int? ISolver.TrySolve(GPUProblem problem)
        {
            if (DidThermalThrottle()) return null;
            _temperature += problem.GpuTemperatureIncrease;
            return problem.Computation();
        }

        private bool DidThermalThrottle()
        {
            if (_temperature <= MaxTemperature) return false;
            Console.WriteLine($"GPU {_model} thermal throttled");
            CoolDown();
            return true;
        }

        private void CoolDown()
        {
            _temperature -= _coolingFactor;
        }
    }
}