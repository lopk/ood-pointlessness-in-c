using System;
using Problems;

namespace Solvers
{
    internal interface ISolver
    {
        protected string Model { get; }

        public sealed void TrySolve(Problem problem)
        {
            if (problem.Solved)
            {
                Console.WriteLine($"{Model}: Problem already solved");
                return;
            }

            problem.TrySolveWithThis(this);
            Console.WriteLine(problem.Solved
                ? $"{Model}: Successfully solved problem {problem.Name}"
                : $"{Model}: Did not solve problem {problem.Name}");
        }

        int? TrySolve(CPUProblem problem) => null;

        int? TrySolve(GPUProblem problem) => null;

        int? TrySolve(NetworkProblem problem) => null;
    }
}