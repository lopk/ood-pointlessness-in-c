using Problems;

namespace Solvers
{
    internal abstract class NetworkDevice : ISolver
    {
        protected readonly string model;
        private int _dataLimit;

        protected NetworkDevice(string model, int dataLimit)
        {
            this.model = model;
            _dataLimit = dataLimit;
        }

        protected string DeviceType { get; set; } = "NetworkDevice";

        string ISolver.Model => $"{model}:{DeviceType}";

        int? ISolver.TrySolve(NetworkProblem problem)
        {
            if (problem.DataToTransfer > _dataLimit) return null;
            if (AdditionalConstraints()) return null;
            _dataLimit -= problem.DataToTransfer;
            return problem.Computation();
        }

        protected virtual bool AdditionalConstraints() => true;
    }
}