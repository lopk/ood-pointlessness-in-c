using System;

namespace Solvers
{
    internal class WiFi : NetworkDevice
    {
        private static readonly Random Rng = new Random(1597);
        private readonly double _packetLossChance;

        public WiFi(string model, int dataLimit, double packetLossChance) : base(model, dataLimit)
        {
            DeviceType = "WiFi";
            _packetLossChance = packetLossChance;
        }

        protected override bool AdditionalConstraints()
        {
            return Rng.NextDouble() < _packetLossChance;
        }
    }
}