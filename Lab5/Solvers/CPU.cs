using Problems;

namespace Solvers
{
    internal class CPU : ISolver
    {
        private readonly int _threads;
        private readonly string _model;

        public CPU(string model, int threads)
        {
            _model = model;
            _threads = threads;
        }

        string ISolver.Model => _model;

        int? ISolver.TrySolve(CPUProblem problem)
        {
            if (problem.RequiredThreads > _threads) return null;
            return problem.Computation();
        }
    }
}