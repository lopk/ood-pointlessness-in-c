﻿namespace MultiPlatform.Interfaces
{
    public interface ITextBox
    {
        string Content { set; }
        void DrawContent();
    }
}