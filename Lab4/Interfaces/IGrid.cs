﻿using System.Collections.Generic;

namespace MultiPlatform.Interfaces
{
    public interface IGrid
    {
        void AddButton(IButton button);

        void AddTextBox(ITextBox textBox);

        IEnumerable<IButton> GetButtons();

        IEnumerable<ITextBox> GetTextBoxes();
    }
}