﻿namespace MultiPlatform.Interfaces
{
    public interface IButton
    {
        string Content { set; }
        void DrawContent();
        void ButtonPressed();
    }
}