﻿using System;
using Lab4.Implementation;
using Lab4.Implementation.AndroidImplementation;
using Lab4.Implementation.iOSImplementation;
using Lab4.Implementation.WindowsImplementation;

namespace Lab4
{
    internal static class Program
    {
        private static void BuildUi(IUiFactory factory)
        {
            var grid = factory.CreateGrid();

            var button1 = factory.CreateButton();
            var button2 = factory.CreateButton();
            var button3 = factory.CreateButton();
            button1.Content = "BigPurpleButton";
            button2.Content = "SmallButton";
            button3.Content = "Baton";
            grid.AddButton(button1);
            grid.AddButton(button2);
            grid.AddButton(button3);

            var textBox1 = factory.CreateTextBox();
            var textBox2 = factory.CreateTextBox();
            var textBox3 = factory.CreateTextBox();
            textBox1.Content = "";
            textBox2.Content = "EmptyTextBox";
            textBox3.Content = "xoBtxeT";
            try
            {
                grid.AddTextBox(textBox1);
                grid.AddTextBox(textBox2);
                grid.AddTextBox(textBox3);
            }
            catch (NotSupportedException e) { }

            foreach (var button in grid.GetButtons())
            {
                button.ButtonPressed();
                button.DrawContent();
            }

            foreach (var textBox in grid.GetTextBoxes()) textBox.DrawContent();
        }

        private static void Main()
        {
            Console.WriteLine("<---------------------iOS--------------------->");
            BuildUi(new IosUiFactory());

            Console.WriteLine("<---------------------Windows--------------------->");
            BuildUi(new WindowsUiFactory());

            Console.WriteLine("<---------------------Android--------------------->");
            BuildUi(new AndroidUiFactory());
        }
    }
}