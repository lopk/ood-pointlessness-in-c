using System;

namespace Lab4.Implementation
{
    internal class BaseClass
    {
        internal BaseClass(string platformName, string objectName)
        {
            Console.WriteLine($"{platformName}{objectName} created");
        }
    }
}