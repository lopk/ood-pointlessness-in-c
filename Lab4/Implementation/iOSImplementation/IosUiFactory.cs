using MultiPlatform.Interfaces;

namespace Lab4.Implementation.iOSImplementation
{
    public class IosUiFactory : IUiFactory
    {
        public IButton CreateButton()
        {
            return new IosButton();
        }

        public IGrid CreateGrid()
        {
            return new IosGrid();
        }

        public ITextBox CreateTextBox()
        {
            return new IosTextBox();
        }
    }
}