using System;
using MultiPlatform.Interfaces;

namespace Lab4.Implementation.iOSImplementation
{
    internal class IosTextBox : BaseClass, ITextBox
    {
        internal IosTextBox() : base("IOS", "TextBox")
        {
            Content = "";
        }

        public string Content { get; set; }

        public void DrawContent()
        {
            Console.WriteLine(Content);
        }
    }
}