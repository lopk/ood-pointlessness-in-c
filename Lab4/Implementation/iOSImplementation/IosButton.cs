using System;
using MultiPlatform.Interfaces;

namespace Lab4.Implementation.iOSImplementation
{
    internal class IosButton : BaseClass, IButton
    {
        internal IosButton() : base("IOS", "Button")
        {
            Content = "Button";
        }

        public string Content { private get; set; }

        public void DrawContent()
        {
            Console.WriteLine(Content);
        }

        public void ButtonPressed()
        {
            Console.WriteLine($"IOS Button pressed, content - {Content}!");
        }
    }
}