using System;
using System.Collections.Generic;
using MultiPlatform.Interfaces;

namespace Lab4.Implementation.iOSImplementation
{
    internal class IosGrid : BaseClass, IGrid
    {
        private readonly List<IButton> _buttons;
        private readonly List<ITextBox> _textBoxes;

        internal IosGrid() : base("IOS", "Grid")
        {
            _buttons = new List<IButton>();
            _textBoxes = new List<ITextBox>();
        }

        public void AddButton(IButton button)
        {
            switch (button)
            {
                case IosButton btn:
                    AddCompliantButton(btn);
                    break;
                default:
                    throw new NotSupportedException();
            }
        }

        public void AddTextBox(ITextBox textBox)
        {
            switch (textBox)
            {
                case IosTextBox txB:
                    _textBoxes.Add(txB);
                    break;
                default:
                    throw new NotSupportedException();
            }
        }

        public IEnumerable<IButton> GetButtons()
        {
            return _buttons;
        }

        public IEnumerable<ITextBox> GetTextBoxes()
        {
            return _textBoxes;
        }

        private void AddCompliantButton(IButton button)
        {
            _buttons.Add(button);
        }
    }
}