using MultiPlatform.Interfaces;

namespace Lab4.Implementation
{
    public interface IUiFactory
    {
        public IButton CreateButton();
        public IGrid CreateGrid();
        public ITextBox CreateTextBox();
    }
}