using System;
using MultiPlatform.Interfaces;

namespace Lab4.Implementation.WindowsImplementation
{
    internal class WindowsTextBox : BaseClass, ITextBox
    {
        private string _content;

        internal WindowsTextBox() : base("Windows", "TextBox")
        {
        }

        public string Content
        {
            private get { return _content ?? "TextBox by .Net Core"; }
            set { _content = value.Substring(value.Length / 2) + " by .Net Core"; }
        }

        public void DrawContent()
        {
            Console.WriteLine(Content);
        }
    }
}