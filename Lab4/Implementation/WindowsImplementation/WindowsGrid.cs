using System;
using System.Collections.Generic;
using System.Linq;
using MultiPlatform.Interfaces;

namespace Lab4.Implementation.WindowsImplementation
{
    internal class WindowsGrid : BaseClass, IGrid
    {
        private readonly Stack<IButton> _buttons;
        private readonly Stack<ITextBox> _textBoxes;
        private ITextBox _firstTextBox;

        internal WindowsGrid() : base("Windows", "Grid")
        {
            _buttons = new Stack<IButton>();
            _textBoxes = new Stack<ITextBox>();
        }

        public void AddButton(IButton button)
        {
            switch (button)
            {
                case WindowsButton btn:
                    AddCompliantButton(btn);
                    break;
                default:
                    throw new NotSupportedException();
            }
        }

        public void AddTextBox(ITextBox textBox)
        {
            switch (textBox)
            {
                case WindowsTextBox txB:
                    AddCompliantTextBox(txB);
                    break;
                default:
                    throw new NotSupportedException();
            }
        }

        public IEnumerable<IButton> GetButtons()
        {
            return _buttons;
        }

        public IEnumerable<ITextBox> GetTextBoxes()
        {
            return _textBoxes.Prepend(_firstTextBox);
        }

        private void AddCompliantButton(IButton button)
        {
            _buttons.Push(button);
        }

        private void AddCompliantTextBox(ITextBox textBox)
        {
            if (_firstTextBox is null)
                _firstTextBox = textBox;
            else _textBoxes.Push(textBox);
        }
    }
}