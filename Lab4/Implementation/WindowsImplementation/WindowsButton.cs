using System;
using MultiPlatform.Interfaces;

namespace Lab4.Implementation.WindowsImplementation
{
    internal class WindowsButton : BaseClass, IButton
    {
        private string _content;

        internal WindowsButton() : base("Windows", "Button")
        {
        }

        public string Content
        {
            private get => _content ?? "BUTTON";
            set => _content = value.ToUpper();
        }

        public void DrawContent()
        {
            Console.WriteLine(Content);
        }

        public void ButtonPressed()
        {
            Console.WriteLine("Windows button pressed");
        }
    }
}