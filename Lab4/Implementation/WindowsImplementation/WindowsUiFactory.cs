using MultiPlatform.Interfaces;

namespace Lab4.Implementation.WindowsImplementation
{
    public class WindowsUiFactory : IUiFactory
    {
        public IButton CreateButton()
        {
            return new WindowsButton();
        }

        public IGrid CreateGrid()
        {
            return new WindowsGrid();
        }

        public ITextBox CreateTextBox()
        {
            return new WindowsTextBox();
        }
    }
}