using System;
using MultiPlatform.Interfaces;

namespace Lab4.Implementation.AndroidImplementation
{
    internal class AndroidTextBox : BaseClass, ITextBox
    {
        private string _content;

        internal AndroidTextBox() : base("Android", "TextBox")
        {
        }

        public string Content
        {
            private get => _content ?? "";
            set
            {
                var chars = value.ToCharArray();
                Array.Reverse(chars);
                _content = new string(chars);
            }
        }

        public void DrawContent()
        {
            Console.WriteLine(Content);
        }
    }
}