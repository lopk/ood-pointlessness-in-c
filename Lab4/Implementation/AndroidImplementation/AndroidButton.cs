using System;
using MultiPlatform.Interfaces;

namespace Lab4.Implementation.AndroidImplementation
{
    internal class AndroidButton : BaseClass, IButton
    {
        private string _content;

        internal AndroidButton() : base("Android", "Button")
        {
        }

        public string Content
        {
            private get => _content ?? "Button";
            set => _content = value.Substring(0, Math.Min(8, value.Length));
        }

        public void DrawContent()
        {
            Console.WriteLine(Content);
        }

        public void ButtonPressed()
        {
            Console.WriteLine($"Sweet {Content}!");
        }
    }
}