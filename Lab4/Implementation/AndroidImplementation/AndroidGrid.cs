using System;
using System.Collections.Generic;
using MultiPlatform.Interfaces;

namespace Lab4.Implementation.AndroidImplementation
{
    internal class AndroidGrid : BaseClass, IGrid
    {
        private readonly List<IButton> _buttons;

        internal AndroidGrid() : base("Android", "Grid")
        {
            _buttons = new List<IButton>();
        }

        public void AddButton(IButton button)
        {
            switch (button)
            {
                case AndroidButton btn:
                    AddCompliantButton(btn);
                    break;
                default:
                    throw new NotSupportedException();
            }
        }

        public void AddTextBox(ITextBox textBox)
        {
            throw new NotSupportedException();
        }

        public IEnumerable<IButton> GetButtons()
        {
            return _buttons;
        }

        public IEnumerable<ITextBox> GetTextBoxes()
        {
            return new List<ITextBox>();
        }

        private void AddCompliantButton(IButton button)
        {
            _buttons.Add(button);
        }
    }
}