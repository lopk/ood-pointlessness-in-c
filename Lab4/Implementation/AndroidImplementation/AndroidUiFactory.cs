using MultiPlatform.Interfaces;

namespace Lab4.Implementation.AndroidImplementation
{
    public class AndroidUiFactory : IUiFactory
    {
        public IButton CreateButton()
        {
            return new AndroidButton();
        }

        IGrid IUiFactory.CreateGrid()
        {
            return new AndroidGrid();
        }

        public ITextBox CreateTextBox()
        {
            return new AndroidTextBox();
        }
    }
}