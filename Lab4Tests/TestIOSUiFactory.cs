using System;
using System.IO;
using System.Linq;
using Lab4.Implementation;
using Lab4.Implementation.iOSImplementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lab4Tests
{
    [TestClass]
    public class TestIosUiFactory
    {
        private StringWriter _consoleCapture;

        [TestInitialize]
        public void SetUp()
        {
            _consoleCapture = new StringWriter();
            Console.SetOut(_consoleCapture);
        }

        [TestMethod]
        public void TestCreateButtonObject()
        {
            IUiFactory factory = new IosUiFactory();
            factory.CreateButton();
            AssertConsoleContains("IOSButton created");
        }

        [TestMethod]
        public void TestCreateTextBoxObject()
        {
            IUiFactory factory = new IosUiFactory();
            factory.CreateTextBox();
            AssertConsoleContains("IOSTextBox created");
        }

        [TestMethod]
        public void TestCreateGridObject()
        {
            IUiFactory factory = new IosUiFactory();
            factory.CreateGrid();
            AssertConsoleContains("IOSGrid created");
        }

        [TestMethod]
        public void TestTextBoxDrawDefaultContent()
        {
            var textBox = new IosUiFactory().CreateTextBox();
            textBox.DrawContent();
            AssertConsoleContains("");
        }

        [TestMethod]
        public void TestTextBoxSetDrawContent()
        {
            var textBox = new IosUiFactory().CreateTextBox();
            char[] str =
                {'x', 'y', 'z', 'G', 'U', 'T'}; // when i tried strings dotnet put some non-printable characters
            textBox.Content = new string(str);
            textBox.DrawContent();
            // ReSharper disable once StringLiteralTypo
            AssertConsoleContains("xyzGUT");
        }

        [TestMethod]
        public void TestButtonDrawDefaultContent()
        {
            var button = new IosUiFactory().CreateButton();
            button.DrawContent();
            AssertConsoleContains("Button");
        }

        [DataTestMethod]
        [DataRow("\babracadabra", "\babracadabra")]
        [DataRow("         like whitespaces?", "         like whitespaces?")]
        [DataRow("\tjest?\t           nie ma", "\tjest?\t           nie ma")]
        public void TestButtonSetContentDrawContent(string input, string expected)
        {
            var button = new IosUiFactory().CreateButton();
            button.Content = input;
            button.DrawContent();
            AssertConsoleContains(expected);
        }

        [TestMethod]
        public void TestButtonPressed()
        {
            var button = new IosUiFactory().CreateButton();
            const string content = "TDD";
            button.Content = content;
            button.ButtonPressed();
            AssertConsoleContains($"IOS Button pressed, content - {content}!");
        }

        [TestMethod]
        public void TestGridGetButtons()
        {
            IUiFactory factory = new IosUiFactory();
            var grid = factory.CreateGrid();
            var btn1 = factory.CreateButton();
            var btn2 = factory.CreateButton();
            var button = factory.CreateButton();
            button.Content = "yes";
            grid.AddButton(btn1);
            grid.AddButton(btn2);
            grid.AddButton(button);
            var buttons = grid.GetButtons();
            CollectionAssert.AreEqual(new[] {btn1, btn2, button},
                buttons.ToArray());
        }

        [TestMethod]
        public void TestGridGetTextBoxes()
        {
            IUiFactory factory = new IosUiFactory();
            var grid = factory.CreateGrid();
            var tx1 = factory.CreateTextBox();
            var tx2 = factory.CreateTextBox();
            var tx3 = factory.CreateTextBox();
            tx3.Content = "yes";
            grid.AddTextBox(tx1);
            grid.AddTextBox(tx2);
            grid.AddTextBox(tx3);
            var textBoxes = grid.GetTextBoxes();
            CollectionAssert.AreEqual(new[]
            {
                tx1, tx2, tx3
            }, textBoxes.ToArray());
        }

        private void AssertConsoleContains(string expected)
        {
            var output = _consoleCapture.ToString();
            Assert.IsTrue(output
                    .Contains(expected), $"Found no {expected} in {output}"
            );
        }
    }
}