using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using Lab4.Implementation;
using Lab4.Implementation.WindowsImplementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lab4Tests
{
    [TestClass]
    public class TestWindowsUiFactory
    {
        private StringWriter _consoleCapture;

        [TestInitialize]
        public void SetUp()
        {
            _consoleCapture = new StringWriter();
            Console.SetOut(_consoleCapture);
        }

        [TestMethod]
        public void TestCreateButtonObject()
        {
            IUiFactory factory = new WindowsUiFactory();
            factory.CreateButton();
            AssertConsoleContains("WindowsButton created");
        }

        [TestMethod]
        public void TestCreateTextBoxObject()
        {
            IUiFactory factory = new WindowsUiFactory();
            factory.CreateTextBox();
            AssertConsoleContains("WindowsTextBox created");
        }

        [TestMethod]
        public void TestCreateGridObject()
        {
            IUiFactory factory = new WindowsUiFactory();
            factory.CreateGrid();
            AssertConsoleContains("WindowsGrid created");
        }

        [TestMethod]
        public void TestTextBoxDrawDefaultContent()
        {
            var textBox = new WindowsUiFactory().CreateTextBox();
            textBox.DrawContent();
            AssertConsoleContains("TextBox by .Net Core");
        }

        [TestMethod]
        public void TestTextBoxSetDrawContent()
        {
            var textBox = new WindowsUiFactory().CreateTextBox();
            char[] str =
                {'x', 'y', 'z', 'G', 'U', 'T'}; // when i tried strings dotnet put some non-printable characters
            textBox.Content = new string(str);
            textBox.DrawContent();
            AssertConsoleContains("GUT by .Net Core");
        }

        [TestMethod]
        public void TestButtonDrawDefaultContent()
        {
            var button = new WindowsUiFactory().CreateButton();
            button.DrawContent();
            AssertConsoleContains("BUTTON");
        }

        [DataTestMethod]
        [DataRow("input", "INPUT")]
        [DataRow("iNpUt5", "INPUT5")]
        [DataRow("A b", "A B")]
        public void TestButtonSetContentDrawContent(string input, string expected)
        {
            var button = new WindowsUiFactory().CreateButton();
            button.Content = input;
            button.DrawContent();
            AssertConsoleContains(expected);
        }

        [TestMethod]
        public void TestButtonPressed()
        {
            var button = new WindowsUiFactory().CreateButton();
            button.ButtonPressed();
            AssertConsoleContains("Windows button pressed");
        }

        [TestMethod]
        public void TestGridGetButtons()
        {
            IUiFactory factory = new WindowsUiFactory();
            var grid = factory.CreateGrid();
            var btn1 = factory.CreateButton();
            var btn2 = factory.CreateButton();
            var button = factory.CreateButton();
            button.Content = "yes";
            grid.AddButton(btn1);
            grid.AddButton(btn2);
            grid.AddButton(button);
            var buttons = grid.GetButtons();
            CollectionAssert.AreEqual(new[]
            {
                button, btn2, btn1
            }, buttons.ToArray());
        }

        [TestMethod]
        public void TestGridGetTextBoxes()
        {
            IUiFactory factory = new WindowsUiFactory();
            var grid = factory.CreateGrid();
            var tx1 = factory.CreateTextBox();
            var tx2 = factory.CreateTextBox();
            var tx3 = factory.CreateTextBox();
            tx3.Content = "yes";
            grid.AddTextBox(tx1);
            grid.AddTextBox(tx2);
            grid.AddTextBox(tx3);
            var textBoxes = grid.GetTextBoxes();
            CollectionAssert.AreEqual(new[]
            {
                tx1, tx3, tx2
            }, textBoxes.ToArray());
        }

        private void AssertConsoleContains([NotNull] string expected)
        {
            var output = _consoleCapture.ToString();
            Assert.IsTrue(output
                    .Contains(expected), $"Found no {expected} in {output}"
            );
        }
    }
}