using System;
using System.IO;
using System.Linq;
using Lab4.Implementation;
using Lab4.Implementation.AndroidImplementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MultiPlatform.Interfaces;

namespace Lab4Tests
{
    [TestClass]
    public class TestAndroidUiFactory
    {
        private StringWriter _consoleCapture;

        [TestInitialize]
        public void SetUp()
        {
            _consoleCapture = new StringWriter();
            Console.SetOut(_consoleCapture);
        }

        [TestMethod]
        public void TestCreateButtonObject()
        {
            IUiFactory factory = new AndroidUiFactory();
            factory.CreateButton();
            AssertConsoleContains("AndroidButton created");
        }

        [TestMethod]
        public void TestCreateTextBoxObject()
        {
            IUiFactory factory = new AndroidUiFactory();
            factory.CreateTextBox();
            AssertConsoleContains("AndroidTextBox created");
        }

        [TestMethod]
        public void TestCreateGridObject()
        {
            IUiFactory factory = new AndroidUiFactory();
            factory.CreateGrid();
            AssertConsoleContains("AndroidGrid created");
        }

        [TestMethod]
        public void TestTextBoxDrawDefaultContent()
        {
            var textBox = new AndroidUiFactory().CreateTextBox();
            textBox.DrawContent();
            AssertConsoleContains("");
        }

        [TestMethod]
        public void TestTextBoxSetDrawContent()
        {
            var textBox = new AndroidUiFactory().CreateTextBox();
            char[] str =
                {'x', 'y', 'z', 'G', 'U', 'T'}; // when i tried strings dotnet put some non-printable characters
            textBox.Content = new string(str);
            textBox.DrawContent();
            // ReSharper disable once StringLiteralTypo
            AssertConsoleContains("TUGzyx");
        }

        [TestMethod]
        public void TestButtonDrawDefaultContent()
        {
            var button = new AndroidUiFactory().CreateButton();
            button.DrawContent();
            AssertConsoleContains("Button");
        }

        [DataTestMethod]
        // ReSharper disable once StringLiteralTypo
        [DataRow("\babracadabra", "\babracad")]
        [DataRow("         like whitespaces?", "        ")]
        [DataRow("\tjest?\t           nie ma", "\tjest?\t ")]
        public void TestButtonSetContentDrawContent(string input, string expected)
        {
            var button = new AndroidUiFactory().CreateButton();
            button.Content = input;
            button.DrawContent();
            AssertConsoleContains(expected);
        }

        [TestMethod]
        public void TestButtonPressed()
        {
            var button = new AndroidUiFactory().CreateButton();
            const string content = "TDD";
            button.Content = content;
            button.ButtonPressed();
            AssertConsoleContains($"Sweet {content}!");
        }

        [TestMethod]
        public void TestGridGetButtons()
        {
            IUiFactory factory = new AndroidUiFactory();
            var grid = factory.CreateGrid();
            var btn1 = factory.CreateButton();
            var btn2 = factory.CreateButton();
            var button = factory.CreateButton();
            button.Content = "yes";
            grid.AddButton(btn1);
            grid.AddButton(btn2);
            grid.AddButton(button);
            var buttons = grid.GetButtons();
            CollectionAssert.AreEqual(new[] {btn1, btn2, button},
                buttons.ToArray());
        }

        [TestMethod]
        public void TestGridGetTextBoxes()
        {
            IUiFactory factory = new AndroidUiFactory();
            var grid = factory.CreateGrid();
            var tx1 = factory.CreateTextBox();
            var tx2 = factory.CreateTextBox();
            tx2.Content = "no";
            // ReSharper disable once ImplicitlyCapturedClosure
            Assert.ThrowsException<NotSupportedException>(() => grid.AddTextBox(tx1));
            // ReSharper disable once ImplicitlyCapturedClosure
            Assert.ThrowsException<NotSupportedException>(() => grid.AddTextBox(tx2));
            var textBoxes = grid.GetTextBoxes();
            CollectionAssert.AreEqual(new ITextBox[0],
                textBoxes.ToArray());
        }

        private void AssertConsoleContains(string expected)
        {
            var output = _consoleCapture.ToString();
            Assert.IsTrue(output
                    .Contains(expected), $"Found no {expected} in {output}"
            );
        }
    }
}